package executors;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class ScheduledExecutorServiceEx {

    public static void main(String[] args) {
        simpleSchedule();
//        scheduleAtFixedRate();
//        scheduleWithFixedDelay();
    }

    private static void scheduleWithFixedDelay(){
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

        Runnable task = () ->
        {
            try {
                TimeUnit.SECONDS.sleep(3);
                System.out.println("Running task...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        ses.scheduleWithFixedDelay(task, 5, 2, TimeUnit.SECONDS);
    }

    private static void scheduleAtFixedRate(){
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

        Runnable task = () ->
            System.out.println("Running task...");


        ses.scheduleAtFixedRate(task, 5, 2, TimeUnit.SECONDS);
    }

    private static void simpleSchedule(){
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

        Runnable task2 = () ->
            System.out.println("Running task2...");

        task1();
        //run this task after 5 seconds, nonblock for task3

        ses.schedule(task2, 5, TimeUnit.SECONDS);
        ses.shutdown();

        task3();
    }

    private static void task1() {
        System.out.println("Running task1...");
    }

    private static void task3() {
        System.out.println("Running task3...");
    }
}
